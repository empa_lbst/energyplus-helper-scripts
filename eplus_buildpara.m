% Build Parametric:SetValueForRun objects
% Generates all combinations of all variable options
% Copy and paste output line into text file, open in idf editor to format

close all; clear all; clc;

objs={};
objs{end+1}.varname = '$Orient'; % variable name in idf
objs{end}.vals = {'0', '90', '180', '270'}; % options this variable can take

objs{end+1}.varname = '$WinHeight'; % variable name in idf
objs{end}.vals = {'2.001', '3', '3'}; % options this variable can take
objs{end}.varname2 = '$WinMat'; % another variable in the same group
objs{end}.vals2 = {'Exterior_Window', 'Exterior_Window', 'WIN-DOUBLE-Aero'}; % options this variable can take

objs{end+1}.varname = '$DCType';
objs{end}.vals = {'1', '2'};
objs{end}.varname2 = '$DCProb';
objs{end}.vals2 = {'1', '0.5'};
objs{end}.varname3 = '$DCSteps';
objs{end}.vals3 = {'0', '2'};

objs{end+1}.varname = '$Lux';
objs{end}.vals = {'300', '500'};

objs{end+1}.varname = '$Sched';
objs{end}.vals = {'Occupancy resi', 'Occupancy all day'};


for i=1:size(objs,2)
    n(i)=size(objs{i}.vals,2); % number of options for each variable
end

n2(1)=n(1);
for i=2:size(objs,2)
    n2(i)=n2(i-1)*n(i); % period of repetition for each variable
end

objs2={};
for i=1:size(objs,2)
    objs2{end+1}.vals=reshape(repmat(objs{i}.vals,n2(i)/n(i),n2(end)/n2(i)),[1 n2(end)]);
    objs2{end}.varname=objs{i}.varname;
    try
        objs2{end+1}.vals=reshape(repmat(objs{i}.vals2,n2(i)/n(i),n2(end)/n2(i)),[1 n2(end)]);
        objs2{end}.varname=objs{i}.varname2;
    end
    try
        objs2{end+1}.vals=reshape(repmat(objs{i}.vals3,n2(i)/n(i),n2(end)/n2(i)),[1 n2(end)]);
        objs2{end}.varname=objs{i}.varname3;
    end
end

output='';
for i=1:size(objs2,2)
    output = [output 'Parametric:SetValueForRun,'];
    output = [output objs2{i}.varname ','];
    
    for j=1:n2(end)
        output = [output objs2{i}.vals{j} ','];
    end
    output(end)=';';
end
output