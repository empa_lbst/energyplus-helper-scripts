%% EPlus Run
% Set up and execute an EnergyPlus model
% repobj.type = 'Shading:Site:Detailed'; % type of object
% repobj.name = '*'; % name of specific object instance, or * for all
% repobj.propno = 3; % property number i.e. line number in idf editor EXCLUDING name
% repobj.propval = val(2); % new value
%%
function out = eplus_run(var,job)
file = 'Office_v1a_584.idf'; % IDF file name

status.err = eplus_build(file,var,job); % edit the IDF file

fprintf(['Running E+ (vars:' repmat(' %g',1,size(var,2)) ')...'],var); % report
tic
% the batch file RunEPlusEdit.bat should point to C:\eplus and C:\eplus\Outputs
[out.status,out.output] = system(['C:\EnergyPlusV8-1-0\RunEPlusEdit.bat ' idffile '_' num2str(job) ' GBR_London.Gatwick.037760_IWEC']);
timetaken = toc;
fprintf(' Done (%g seconds).\n',timetaken); % report


zones = importdata('C:\eplus\zonenames.csv');
outvars = importdata('C:\eplus\outvars.csv');
s={};
for j=1:size(outvars,1)
    for i=1:size(zones,1)
        s{end+1} = [zones{i} outvars{j}];
    end
end
s{end+1} = 'CORE_BOTTOM:Zone Hot Water Equipment Total Heating Energy [J](Hourly)';

[~,data] = eplus_readcsv(['C:\eplus\Outputs\' idffile '_' num2str(job) '.csv'],s);
sumdata = [sum(data(:,1:30),2)/1000 sum(data(:,46:60),2)/3600000 sum(data(:,[31:45 61]),2)/3600000]; % sum loads: elec, cool, heat
fprintf('EPlus results read (Elec: %g, Cool: %g, Heat: %g).\n',sum(sumdata)); % report number of replacements

out.job = job;
out.data = sumdata;
out.timetaken = timetaken;
end

