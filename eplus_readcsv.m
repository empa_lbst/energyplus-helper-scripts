%% EPlus Read csv
% Read the values from an EnergyPlus output file
% s{} contains exact column headings
%%
function [A, D] = eplus_readcsv(filename,s)
A = importdata(filename); % read in file
%labs = strsplit(A.textdata{1,1},','); % split first line to get column labels % not needed if matrix is full as importdata gets the columns right
labs = A.textdata(1,:); % column labels
labs(1)=[]; % get rid of first column so the labels line up with the data

for i=1:size(s,2) % for each cell in cell array s
    D(:,i)=A.data(:,strcmpi(strtrim(s{i}), strtrim(labs))); % get data from the column with the matching heading
end

end