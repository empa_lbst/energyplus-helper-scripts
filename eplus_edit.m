%% EPlus Edit
% Edit an idf file by finding and replacing elements as listed in repobj
% repobj.type = 'Shading:Site:Detailed'; % type of object
% repobj.name = '*'; % name of specific object instance, or * for all
% repobj.propno = 3; % property number i.e. line number in idf editor EXCLUDING name
% repobj.propval = val(2); % new value
%%
function err = eplus_edit(repobj,idffile,job)

infile = ['C:\eplus\' idffile '.idf']; % new file
outfile = ['C:\eplus\to_run\' idffile '_' num2str(job) '.idf']; % new file
if exist(outfile,'file')
    delete(outfile); % get rid of old outfile
end
try
    fid = fopen(infile,'r+');
catch
    fprintf('ERROR: FILE NOT FOUND\n');
    err=-1;
end

lines=[];
while feof(fid)==0
    lines{end+1}=fgetl(fid); % get all lines
end
fclose(fid);

newlines = lines;
rc=0;
for k=1:size(repobj,2)
    ind=[];
    n=length(lines);
    for i=1:n
        if strcmp(strtrim(lines{i}(1:end-1)),repobj(k).type) % find type e.g. Shading:Building:Detailed
            ind(end+1)=i; % ind takes the line number of the beginning of the object
        end
    end
    
    wrongind=[];
    if repobj(k).name ~= '*' % if not all objects
        for i=1:length(ind)
            [name,~] = strsplit(lines{ind(i)+1},','); % split first line to ge
            if ~strcmp(strtrim(name),repobj(k).name); % if wrong object name
                wrongind(end+1)=i; % add to list to remove
            end
        end
    end
    ind(wrongind)=[]; % remove wrong objects
    
    for i=1:length(ind)
        rc=rc+1;
        replaceline = ind(i) + repobj(k).propno;
        if strfind(lines{replaceline},';') % if end of object
            endstr = ';'; % use correct line end
        else
            endstr = ',';
        end

        if isa(repobj(k).propval,'char')
            newlines{replaceline} = [repobj(k).propval endstr]; % replace value
        else
            newlines{replaceline} = [num2str(repobj(k).propval) endstr]; % replace value
        end
    end
end

fid = fopen(outfile, 'w');
for i=1:length(lines)
    fprintf(fid, '%s \r\n',newlines{i});
end
fclose(fid);

fprintf('E+ file edited, %g replacements.\n',rc); % report
err=0;
