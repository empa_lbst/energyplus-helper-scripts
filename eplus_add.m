% Build an idf file by adding some objects to a template

close all; clear all; clc
newfile = 'eplus\layoutv2d.idf'; % new file
tempfile = 'eplus\layoutv2d_nogeometry.idf'; % template file containing whatever is not added here

copyfile(tempfile,newfile);
fileID = fopen(newfile,'a'); % append to end of file

zonenames = {'A','B','C'};
xoffs = [0 5 10];
yoffs = [0 0 0];
zoffs = [0 0 0];

for i=1:3
    % Format for the Zone object: Name, Orientation, X, Y, Z, [...];
    fprintf(fileID,'%s\r\n',['Zone, ' zonenames{i} ', 0, ' num2str(xoffs(i)) ', ' num2str(yoffs(i)) ', ' num2str(zoffs(i)) ';']);
end
fclose(fileID);