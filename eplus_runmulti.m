v2struct(params); % unpack parameters

[~,hostname]= system('hostname');
if strcmp(strtrim(hostname),'DDM04394') % if Dell workstation
    projectpath = 'C:\Users\evr\Documents\energy_hub\Initial model\'; % project directory
else
    projectpath = 'C:\Users\evr\Google Drive\energy_hub\Initial model\'; % project directory
end

fprintf('Running E+...');
curdir = cd;
cd('C:\eplus\to_run'); % move to eplus/to_run
tic
[status.run,out] = system(['RunDirMulti.bat GBR_London.Gatwick.037760_IWEC ' num2str(nparallel)]); % use the multi-processor batch file
esc = 0;
while esc == 0 % loop until all eplus jobs have completed
    d = dir('C:\eplus\to_run\*.err'); % get all .err files in dir
    if size(d,1) == size(to_run,2), esc = 1; end % if the total matches the number of runs, escape
end
timetaken = toc;
cd(curdir); % move back to home dir
fprintf(' Done (%g seconds).\n',timetaken);

zones = importdata('C:\eplus\zonenames.csv'); % zone names to extract
outvars = importdata('C:\eplus\outvars.csv'); % variable names to extract
s={};
for j=1:size(outvars,1)
    for i=1:size(zones,1)
        s{end+1} = [zones{i} outvars{j}]; % build cell array of zonename varname
    end
end
s{end+1} = 'CORE_BOTTOM:Zone Hot Water Equipment Total Heating Energy [J](Hourly)'; % add hot water variable (not per zone)
for i=1:size(to_run,2) % for each solution run
    [~,data] = eplus_readcsv(['C:\eplus\to_run\' idffile '_' num2str(to_run(i)) '.csv'],s); % read data from csv output file
    sumdata = [sum(data(:,1:30),2)/1000 sum(data(:,46:60),2)/3600000 sum(data(:,[31:45 61]),2)/3600000]; % sum loads: elec, cool, heat (inc DHW)
    fprintf('EPlus results read (Elec: %g, Cool: %g, Heat: %g).\n',sum(sumdata)); % report results read
    eplus_sols.data{to_run(i)} = sumdata; % add results to DB
end

movefile('C:\eplus\to_run\*.csv','C:\eplus\all'); % move results files
movefile('C:\eplus\to_run\*.idf','C:\eplus\all'); % move idf files
delete(['C:\eplus\to_run\' idffile '_*']); % clear \to_run folder
rmdir('C:\eplus\to_run\tempsim*','s'); % remove temporary directories from batch file

