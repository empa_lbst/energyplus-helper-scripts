%% EPlus Run
% Set up an EnergyPlus model
% repobj.type = 'Shading:Site:Detailed'; % type of object
% repobj.name = '*'; % name of specific object instance, or * for all
% repobj.propno = 3; % property number i.e. line number in idf editor
% repobj.propval = val(2); % new value
%%
function err = eplus_build(file,var,job)
if nargin < 3 % if no job number
    d = dir(['C:\eplus\' idffile '_*.idf']); % find existing .idf files from previous runs
    job = size(d,1)+1;
end

% For this example:
% var =
% Insulation type (1-3)
% North window height steps (0-3)
% East window height steps (0-3)
% South window height steps (0-3)
% West window height steps (0-3)
% Glazing type (1-3)
% North shading level (0-2)
% East shading level (0-2)
% South shading level (0-2)
% West shading level (0-2)

win_types = {'WIN_AVE','WIN_MOD','WIN_PAS'};
ins_thicks = [0.03 0.2 0.5];
wintype = win_types(var(6));
ins = ins_thicks(var(1));

repobj(1) = struct('type','Material','name','Ins','propno',3,'propval',ins); % Insulation thickness
repobj(end+1) = struct('type','Window','name','BN','propno',10,'propval',0.5+0.5*var(2)); % Window height
repobj(end+1) = struct('type','Window','name','MN','propno',10,'propval',0.5+0.5*var(2)); % Window height
repobj(end+1) = struct('type','Window','name','TN','propno',10,'propval',0.5+0.5*var(2)); % Window height
repobj(end+1) = struct('type','Window','name','BE','propno',10,'propval',0.5+0.5*var(3)); % Window height
repobj(end+1) = struct('type','Window','name','ME','propno',10,'propval',0.5+0.5*var(3)); % Window height
repobj(end+1) = struct('type','Window','name','TE','propno',10,'propval',0.5+0.5*var(3)); % Window height
repobj(end+1) = struct('type','Window','name','BS','propno',10,'propval',0.5+0.5*var(4)); % Window height
repobj(end+1) = struct('type','Window','name','MS','propno',10,'propval',0.5+0.5*var(4)); % Window height
repobj(end+1) = struct('type','Window','name','TS','propno',10,'propval',0.5+0.5*var(4)); % Window height
repobj(end+1) = struct('type','Window','name','BW','propno',10,'propval',0.5+0.5*var(5)); % Window height
repobj(end+1) = struct('type','Window','name','MW','propno',10,'propval',0.5+0.5*var(5)); % Window height
repobj(end+1) = struct('type','Window','name','TW','propno',10,'propval',0.5+0.5*var(5)); % Window height

repobj(end+1) = struct('type','Window','name','BN','propno',2,'propval',wintype); % Window height
repobj(end+1) = struct('type','Window','name','MN','propno',2,'propval',wintype); % Window height
repobj(end+1) = struct('type','Window','name','TN','propno',2,'propval',wintype); % Window height
repobj(end+1) = struct('type','Window','name','BE','propno',2,'propval',wintype); % Window height
repobj(end+1) = struct('type','Window','name','ME','propno',2,'propval',wintype); % Window height
repobj(end+1) = struct('type','Window','name','TE','propno',2,'propval',wintype); % Window height
repobj(end+1) = struct('type','Window','name','BS','propno',2,'propval',wintype); % Window height
repobj(end+1) = struct('type','Window','name','MS','propno',2,'propval',wintype); % Window height
repobj(end+1) = struct('type','Window','name','TS','propno',2,'propval',wintype); % Window height
repobj(end+1) = struct('type','Window','name','BW','propno',2,'propval',wintype); % Window height
repobj(end+1) = struct('type','Window','name','MW','propno',2,'propval',wintype); % Window height
repobj(end+1) = struct('type','Window','name','TW','propno',2,'propval',wintype); % Window height

repobj(end+1) = struct('type','WindowProperty:ShadingControl','name','Blinds_N','propno',6,'propval',250+var(7)*250); % Window height
repobj(end+1) = struct('type','WindowProperty:ShadingControl','name','Blinds_E','propno',6,'propval',250+var(8)*250); % Window height
repobj(end+1) = struct('type','WindowProperty:ShadingControl','name','Blinds_S','propno',6,'propval',250+var(9)*250); % Window height
repobj(end+1) = struct('type','WindowProperty:ShadingControl','name','Blinds_W','propno',6,'propval',250+var(10)*250); % Window height

err = eplus_edit(repobj,file,job); % edit idf
end

