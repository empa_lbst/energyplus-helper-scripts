close all; clear all; clc
CHTCs=1;
CPs=1;

bu = 'AAAAAAAAABBBBBBBBB'; % building id
fl = 'BBBMMMTTTBBBMMMTTT'; % floor id: bottom middle top
bl = 'LCRLCRLCRLCRLCRLCR'; % block id: left centre right
if CHTCs
    newfile = 'eplus\layoutv3e.idf';
else
    newfile = 'eplus\layoutv3e_nochtc.idf';
end
copyfile('eplus\layoutv3d_nogeom.idf',newfile); % starting template file
fileID = fopen(newfile,'a');

zones = 'ZoneList, AllZones';
for i=1:18 % for each zone
    zonename = [bu(i) '-' fl(i) '-' bl(i)]; % zone id e.g. A-B-L
    zones = [zones ', ' zonename]; % add to ZoneList
    
    if fl(i)=='B'
        zoff=0; % height offset determined by floor
    elseif fl(i)=='M'
        zoff=3.33;
    else
        zoff=6.66;
    end
    
    if bl(i)=='L'
        xoff=0; % x offset determined by block
        nnorth='1'; % id of north facet
        nsouth='7'; % id of south facet
    elseif bl(i)=='C'
        xoff=10;
        nnorth='2';
        nsouth='6';
    else
        xoff=20;
        nnorth='3';
        nsouth='5';
    end
    
    if bu(i)=='B'
        yoff=0; % y offset determined by building
    else
        yoff=20;
    end
    
    fprintf(fileID,'%s\r\n',['Zone, ' zonename ', 0, ' num2str(xoff) ', ' num2str(yoff) ', ' num2str(zoff) ';']); % zone definition
    
    if fl(i) == 'T' % if top floor, exposed roof
        fprintf(fileID,'%s\r\n',['BuildingSurface:Detailed, Roof-' zonename ', Roof, ExtRoofConst, ' zonename ', Outdoors, , SunExposed, WindExposed, , , 10, 0, 3.33, 10, 10, 3.33, 0, 10, 3.33, 0, 0, 3.33;']);
    else
        fprintf(fileID,'%s\r\n',['BuildingSurface:Detailed, Roof-' zonename ', Roof, IntRoofConst, ' zonename ', Adiabatic, , NoSun, NoWind, , , 10, 0, 3.33, 10, 10, 3.33, 0, 10, 3.33, 0, 0, 3.33;']);
    end
    
    if fl(i) == 'B' % if ground floor, exposed floor
        fprintf(fileID,'%s\r\n',['BuildingSurface:Detailed, Floor-' zonename ', Floor, ExtFloorConst, ' zonename ', Ground, , NoSun, NoWind, , , 0,10,0,10,10,0,10,0,0,0,0,0;']);
    else
        fprintf(fileID,'%s\r\n',['BuildingSurface:Detailed, Floor-' zonename ', Floor, IntFloorConst, ' zonename ', Adiabatic, , NoSun, NoWind, , , 0,10,0,10,10,0,10,0,0,0,0,0;']);
    end
    
    surfname = [bu(i) nnorth fl(i)]; % surf id = building facet floor, e.g. A1B
    fprintf(fileID,'%s\r\n',['BuildingSurface:Detailed, ' surfname ', Wall, ExtWallConst, ' zonename ', Outdoors, , SunExposed, WindExposed, , ,10,10,3.33,10,10,0,0,10,0,0,10,3.33;']); % north wall
    fprintf(fileID,'%s\r\n',['Window, Win' surfname ', WinConst, ' surfname ', , , 1, 2, 1, 6, 1.33;']); % north window
    surfname = [bu(i) nsouth fl(i)];
    fprintf(fileID,'%s\r\n',['BuildingSurface:Detailed, ' surfname ', Wall, ExtWallConst, ' zonename ', Outdoors, , SunExposed, WindExposed, , ,0,0,3.33,0,0,0,10,0,0,10,0,3.33;']); % south wall
    fprintf(fileID,'%s\r\n',['Window, Win' surfname ', WinConst, ' surfname ', , , 1, 2, 1, 6, 1.33;']); % south window
    if bl(i)=='R' % if right-hand block
        surfname = [bu(i) '4' fl(i)];
        fprintf(fileID,'%s\r\n',['BuildingSurface:Detailed, ' surfname ', Wall, ExtWallConst, ' zonename ', Outdoors, , SunExposed, WindExposed, , ,10,0,3.33,10,0,0,10,10,0,10,10,3.33;']);
        fprintf(fileID,'%s\r\n',['Window, Win' surfname ', WinConst, ' surfname ', , , 1, 2, 1, 6, 1.33;']);
    else % interior east wall
        fprintf(fileID,'%s\r\n',['BuildingSurface:Detailed, EastWall-' zonename ', Wall, IntWallConst, ' zonename ', Adiabatic, , NoSun, NoWind, , ,10,0,3.33,10,0,0,10,10,0,10,10,3.33;']);
    end
    if bl(i)=='L' % if left-hand block
        surfname = [bu(i) '8' fl(i)];
        fprintf(fileID,'%s\r\n',['BuildingSurface:Detailed, ' surfname ', Wall, ExtWallConst, ' zonename ', Outdoors, , SunExposed, WindExposed, , ,0,10,3.33,0,10,0,0,0,0,0,0,3.33;']);
        fprintf(fileID,'%s\r\n',['Window, Win' surfname ', WinConst, ' surfname ', , , 1, 2, 1, 6, 1.33;']);
    else % interior west wall
        fprintf(fileID,'%s\r\n',['BuildingSurface:Detailed, WestWall-' zonename ', Wall, IntWallConst, ' zonename ', Adiabatic, , NoSun, NoWind, , ,0,10,3.33,0,10,0,0,0,0,0,0,3.33;']);
    end
    
    fprintf(fileID,'%s\r\n',['HVACTemplate:Zone:IdealLoadsAirSystem,' zonename ',Therm,, 50, 13, .0156, .0077,NoLimit,,,NoLimit,,,,,ConstantSensibleHeatRatio, .7, 60,None, 30,None, .00944,,,,None,NoEconomizer,None, .7, .65;']);
    
    if CPs
        fprintf(fileID,'%s\r\n',['AirflowNetwork:MultiZone:Zone, ' zonename ', Temperature, WindowVentSched, 0.3, 5.0, 10.0, 0.0, 300000.0;']);
    end
end
fprintf(fileID,'%s\r\n',[zones ';']);

if CHTCs
    fls='BMT'; % floor id: bottom middle top
    bus='AB'; % building id
    c=0;
    prog{1} = 'EnergyManagementSystem:Program, SetCHTC'; % EMS prog: 1 local wind speed schedule per surf, read in as sensor, assign to actuator for window and wall
    for i3=1:2 % for each building
        for i=1:8 % for each facet
            for i2=1:3 % for each floor
                surfname = [bus(i3) num2str(i) fls(i2)]; % surf id = building facet floor, e.g. A1B
                c=c+1;
                fprintf(fileID,'%s\r\n',['EnergyManagementSystem:Actuator, Act' surfname ',' surfname ',Surface,Outdoor Air Wind Speed;']);
                fprintf(fileID,'%s\r\n',['EnergyManagementSystem:Actuator, ActWin' surfname ', Win' surfname ',Surface,Outdoor Air Wind Speed;']);
                %fprintf(fileID,'%s\r\n',['EnergyManagementSystem:Actuator, ActWin' surfname ',' surfname ',Surface,Exterior Surface Convection Heat Transfer Coefficient;']);
                fprintf(fileID,'%s\r\n',['EnergyManagementSystem:Sensor, Sens' surfname ',Sens' surfname ',Schedule Value;']);
                fprintf(fileID,'%s\r\n',['Schedule:File, Sens' surfname ',Real,newwindspeeds.csv, ' num2str(c) ',0,8760,Comma,No,15;']);
                prog{end+1}=['SET Act' surfname ' = Sens' surfname];
                prog{end+1}=['SET ActWin' surfname ' = Sens' surfname];
                %prog{end+1}=['SET ActWin' surfname ' = 5'];
            end
        end
    end
    fprintf(fileID,'%s\r\n','EnergyManagementSystem:ProgramCallingManager,RunCHTC,BeginTimestepBeforePredictor,SetCHTC;');
    
    for i=1:size(prog,2)-1
        fprintf(fileID,'%s\r\n',[prog{i} ',']);
    end
    fprintf(fileID,'%s\r\n',[prog{end} ';']);
end

if CPs
    WPCA = 'AirflowNetwork:MultiZone:WindPressureCoefficientArray, Every Degree, 0';
    for WPCi=1:38
        WPCA=[WPCA ', ' num2str(WPCi)];
    end
    fprintf(fileID,'%s\r\n',[WPCA ';']);
    
    for i3=1:2 % for each building
        for i=1:8 % for each facet
            for i2=1:3 % for each floor
                surfname = [bus(i3) num2str(i) fls(i2)]; % surf id = building facet floor, e.g. A1B
                fprintf(fileID,'%s\r\n',['AirflowNetwork:MultiZone:Surface, ' surfname ', CR-1, ' surfname 'Facade, 1.0;']);
                fprintf(fileID,'%s\r\n',['AirflowNetwork:MultiZone:Surface, Win' surfname ', WiOpen1, ' surfname 'Facade, 0.5;']);
                fprintf(fileID,'%s\r\n',['AirflowNetwork:MultiZone:ExternalNode, ' surfname 'Facade, 0, ' surfname 'Facade_WPCValue;']);
                
                WPCV = ['AirflowNetwork:MultiZone:WindPressureCoefficientValues, ' surfname 'Facade_WPCValue, Every Degree'];
                for WPCi=1:38
                    WPCV=[WPCV ', ' num2str(sin(WPCi*pi()/180))];
                end
                fprintf(fileID,'%s\r\n',[WPCV ';']);
            end
        end
    end
end


fclose(fileID);